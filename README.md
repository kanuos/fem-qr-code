# Frontend Mentor - QR code component solution


### About Project
This is a solution to the [QR code component challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/qr-code-component-iux_sIO_H). 

This project uses **HTML** and **vanilla CSS** to implement a *mobile first*, *responsive* design. The screenshot of the finished product can be seen below.

![Design screenshot for the QR code component coding challenge](./Screenshot.png)


### Tech stack:

- [x] HTML to create the structure
- [x] vanilla CSS to design the webpage
- [x] Grid layouts to implement the *block* sections  viz. `main`, `qr code component`
- [x] Flexbox to structure the `qr code component` texts
- [x] BEM naming convention in writing CSS


### Process

- Firstly, to start off the project, I navigated to a newly created directory and created an empty git repo inside using `git init` command. The **FrontEnd Mentor QR code challenge** repository is cloned in the created repo.
- Next, all the unused files, inline CSS etc were removed. **Outfit** font family was added to the project using *Google Fonts CDN*. All the colors provided in the *style-guide.md* was added to the `:root` of the CSS. 
- A CSS reset was implemented. Font size of the page was set to 15px so that `1rem == 15px`
- The main page sanning the full viewport with appropriate background color was implemented. 
- The `QR` code wrapper card component was implemented using *mobile first* design. Grid layout was implemented for the contents of the card. The contents were added keeping *responsive design* in mind.
- Added media query to look according to the `designs` using `375px` for `mobile` and `1440px` for `desktop` breakpoints. The static page was tested on various viewports/devices using the Firefox mobile layout viewer. 
- A screenshot of the final result was taken using **Firefox screenshot** and was added to the project.


### Tools used

- VS Code as the Code Editor
- Live server VS Code extension to create a dev server
- Prettier pluggin for code formatting


### Links

- Solution URL: [Gitlab](https://gitlab.com/kanuos/fem-qr-code)
- Live Site URL: [Netlify](https://serene-baklava-27c794.netlify.app/)



### Author

- Frontend Mentor - [@kanuos](https://www.frontendmentor.io/profile/kanuos)
- GitHub - [@ykanuos](https://github.com/kanuos)
- GitHub - [@ykanuos](https://gitlab.com/kanuos)

